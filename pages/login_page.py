from pages.base_page import BasePage


class LoginPage(BasePage):

    def navigate_to_login_page(self, base_url):
        self._webdriver.navigate_to(base_url + '/login')

    def set_email(self, email):
        email_txt = self._webdriver.find_by_id('user')
        email_txt.send_keys(email)

    def set_password(self, password):
        password_txt = self._webdriver.find_by_id('password')
        password_txt.send_keys(password)

    def click_login_with_atlassian(self):
        login_btn = self._webdriver.find_by_xpath('//input[@value="Log in with Atlassian"]')
        login_btn.click()

    def click_login(self):
        login_btn = self._webdriver.find_by_id('login-submit')
        login_btn.click()
