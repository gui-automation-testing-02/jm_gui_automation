import time

from pages.base_page import BasePage


class BoardsPage(BasePage):

    def click_create(self):
        create_btn = self._webdriver.find_by_xpath('//button[@aria-label="Create board or Workspace"]')
        create_btn.click()

    def click_create_board(self):
        create_board_btn = self._webdriver.find_by_xpath('//span[text()="Create board"]')
        create_board_btn.click()

    def set_title(self, title):
        title_tbx = self._webdriver.find_by_xpath('//input[@data-test-id="create-board-title-input"]')
        title_tbx.send_keys(title)

    def select_visibility(self, option):
        visibility_cmb = self._webdriver.find_by_xpath('//div[contains(@id, "create-board-select-visibility")]')
        visibility_cmb.click()

        new_opt = self._webdriver.find_by_xpath(f'//div[contains(@id, "react-select")]/li/div/div[text()="{option}"]')
        new_opt.click()

    # TODO: Improve this method
    def click_create_on_popover(self):
        create_btn = self._webdriver.find_by_xpath('//button[@data-test-id="create-board-submit-button"]')
        create_btn.click()

    def get_board_title(self):
        header_txt = self._webdriver.find_by_xpath('//h1[contains(@class, "board-header-btn-text")]')
        return header_txt.text
