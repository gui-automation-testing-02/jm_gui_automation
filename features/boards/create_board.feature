Feature: Create Board

  Scenario: Verify it is possible to create a new board
    Given the user is logged in Trello
    When the user clicks Create
      And clicks Create Board
      And types "QAAuto-{{random_string}}" in Board title as "title"
      And selects "Private" in Visibility
      And clicks Create on Create board popover
    Then the user should see "{{title}}" in the board header
      And should see the board "{{title}}" from the REST API
        |key                  |value  |
        |prefs.permissionLevel|private|
      And deletes the board "{{title}}" from the REST API
