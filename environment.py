import allure
from allure_commons.types import AttachmentType

from helpers.selenium.webdriver_manager import WebDriverManager


def before_all(context):
    context.browser = context.config.userdata['browser']
    context.timeout_in_seconds = context.config.userdata['timeout_in_seconds']
    context.remote_url = context.config.userdata['remote_url']
    context.remote_provider = context.config.userdata['remote_provider']
    context.execution_type = context.config.userdata['execution_type']

    context.webapp_base_url = context.config.userdata['webapp_base_url']
    context.webapp_email = context.config.userdata['webapp_email']
    context.webapp_password = context.config.userdata['webapp_password']

    context.restapi_base_url = context.config.userdata['restapi_base_url']
    context.restapi_token = context.config.userdata['restapi_token']
    context.restapi_key = context.config.userdata['restapi_key']


def before_scenario(context, scenario):
    context.params = {'key': context.restapi_key, 'token': context.restapi_token}
    context.webdriver = WebDriverManager(context.browser, context.timeout_in_seconds,
                                         context.execution_type, context.remote_provider, context.remote_url)


def after_scenario(context, scenario):
    context.webdriver.close()


def after_step(context, step):
    if step.status == 'failed':
        allure.attach(context.webdriver.get_screenshot_as_png(), 'screenshot', AttachmentType.PNG)


def after_all(context):
    pass
