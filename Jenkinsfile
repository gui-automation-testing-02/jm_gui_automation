pipeline {

	agent { label 'built-in' }


    parameters {
        choice(choices: 'chrome\nfirefox\nMicrosoftEdge', description: 'any description', name: 'BROWSER')
        choice(choices: 'selenium_grid\nlambda_test\nsauce_labs', description: 'any description', name: 'REMOTE_PROVIDER')
        // Environment
    }

    environment {
        // This is always remote
        EXECUTION_TYPE = 'remote'

        // Thi can be configured based on the parameter REMOTE_PROVIDER
        REMOTE_URL = 'http://10.0.0.9:4444'
    }

    stages {

        stage('GUI Automated Test') {

			agent {
				docker {
					image 'python:3.11-rc'
				}
			}

			stages {
					stage("Install requirements") {
						steps {
							sh '''
								python -m venv .venv
								. .venv/bin/activate
								pip install -r requirements.txt
							'''
						}
                    }

					stage ('Run unit tests') {
						steps {
							sh 'echo "Running unit test"'
						}
					}

					stage ('Run GUI tests') {

						steps {
							sh '''
								. .venv/bin/activate
								behave -D browser=${BROWSER} \
								-D remote_provider=${REMOTE_PROVIDER} \
								-D execution_type=${EXECUTION_TYPE} \
								-D remote_url=${REMOTE_URL} \
								-f allure_behave.formatter:AllureFormatter -o allure-results
							'''
							stash name: 'results', includes: "allure-results/*"

							sh 'hostname'
						}
					}
            }
        }
    }

    post {
        always {
			unstash 'results'
			sh 'hostname'
			script {
				allure([
					includeProperties: false,
					jdk: '',
					properties: [],
					reportBuildPolicy: 'ALWAYS',
					results: [[path: 'results']]
				])
			}
        }
    }
}