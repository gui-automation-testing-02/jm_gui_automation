import time

from behave import *

from helpers import context_helper, requests_helper
from pages.boards_page import BoardsPage

use_step_matcher('re')


@step('(?:the user )?clicks Create')
def click_create(context):
    boards_page = BoardsPage(context.webdriver)
    boards_page.click_create()


@step('(?:the user )?clicks Create Board')
def click_create_board(context):
    boards_page = BoardsPage(context.webdriver)
    boards_page.click_create_board()


@step('(?:the user )?types "(?P<title>.*?)" in Board title(?: as "(?P<variable>.*?)")?')
def set_title(context, title, variable):
    title = context_helper.replace_all(title, context)
    boards_page = BoardsPage(context.webdriver)
    boards_page.set_title(title)
    if variable is not None:
        setattr(context, variable, title)


@step('(?:the user )?selects "(?P<option>.*?)" in Visibility')
def select_visibility(context, option):
    boards_page = BoardsPage(context.webdriver)
    boards_page.select_visibility(option)


@step('(?:the user )?clicks Create on Create board popover')
def click_create(context):
    boards_page = BoardsPage(context.webdriver)
    boards_page.click_create_on_popover()


@then('(?:the user )?should see "(?P<expected_title>.*?)" in the board header')
def set_title(context, expected_title):
    expected_title = context_helper.replace_all(expected_title, context)
    boards_page = BoardsPage(context.webdriver)
    actual_title = boards_page.get_board_title()
    assert expected_title == actual_title


@step('(?:the user )?deletes the board "(?P<title>.*?)" from the REST API')
def delete_board(context, title):
    title = context_helper.replace_all(title, context)
    url = context.restapi_base_url + '/members/me/boards'
    response = requests_helper.get_request(url, context.params)
    response_as_dict = response.json()
    for item in response_as_dict:
        if item['name'] == title:
            board_id = item['id']
            url = context.restapi_base_url + '/boards/' + board_id
            requests_helper.delete_request(url, context.params)


@then('(?:the user )?should see the board "(?P<title>.*?)" from the REST API')
def verify_board(context, title):
    title = context_helper.replace_all(title, context)
    url = context.restapi_base_url + '/members/me/boards'
    response = requests_helper.get_request(url, context.params)
    response_as_dict = response.json()
    for item in response_as_dict:
        if item['name'] == title:
            for (key, expected_value) in context.table:
                actual_value = requests_helper.get_value_from_dict(key, item)
                assert actual_value == expected_value
            break


