import chromedriver_autoinstaller
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from helpers.constants import WebBrowserType


class WebDriverManager:

    def __init__(self, browser, timeout_in_seconds, execution_type, remote_provider, remote_url):
        self._webdriver = None

        if execution_type == 'local':
            match browser:
                case WebBrowserType.CHROME:
                    self.init_local_chrome_driver()
                case WebBrowserType.FIREFOX:
                    self.init_local_firefox_driver()
                case _:
                    raise Exception(f'Invalid browser {browser}!')
        elif execution_type == 'remote':
            match remote_provider:
                case 'selenium_grid':
                    self.init_remote_selenium_grid_driver(browser, remote_url)
                case 'lambda_test':
                    pass
                case 'sauce_labs':
                    self.init_remote_sauce_labs_driver(browser, remote_url)
                case _:
                    raise Exception(f'Invalid remote provider {remote_provider}!')
        else:
            raise Exception(f'Invalid execution type {execution_type}!')

        self._webdriver.implicitly_wait(timeout_in_seconds)
        self._webdriver_wait = WebDriverWait(self._webdriver, timeout_in_seconds)
        self.maximize()

    def init_remote_selenium_grid_driver(self, browser, remote_url):
        self._webdriver = webdriver.Remote(command_executor=remote_url,
                                           desired_capabilities={'browserName': browser, 'javascriptEnabled': True})

    def init_remote_sauce_labs_driver(self, browser, remote_url):
        capabilities = {
            'browserName': browser,
            'browserVersion': 'latest',
            'platformName': 'Windows 10',
            'sauce:options': {

            }
        }
        self._webdriver = webdriver.Remote(command_executor=remote_url, desired_capabilities=capabilities)

    def init_local_chrome_driver(self):
        chromedriver_autoinstaller.install()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_experimental_option('prefs', {'intl.accept_languages': 'en,en_US'})
        self._webdriver = webdriver.Chrome(chrome_options=chrome_options)

    def init_local_firefox_driver(self):
        pass

    def maximize(self):
        self._webdriver.maximize_window()

    def navigate_to(self, url):
        self._webdriver.get(url)

    def close(self):
        self._webdriver.quit()

    def find_by_id(self, id):
        return self._webdriver_wait.until(expected_conditions.visibility_of_element_located((By.ID, id)))
        #return self._webdriver.find_element(By.ID, id)

    def find_by_xpath(self, xpath):
        return self._webdriver_wait.until(expected_conditions.visibility_of_element_located((By.XPATH, xpath)))
        #return self._webdriver.find_element(By.XPATH, xpath)

    def wait_for_element_to_be_clickable(self, element):
        self._webdriver_wait.until(expected_conditions.element_to_be_clickable(element))

    def get_screenshot_as_png(self):
        return self._webdriver.get_screenshot_as_png()
