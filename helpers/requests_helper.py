import json

import requests


def get_request(url, params_as_json):
    return requests.get(url, params=params_as_json)


def delete_request(url, params_as_json):
    return requests.delete(url, params=params_as_json)


# Replaced by response.json()
def convert_string_to_dict(obj_as_string):
    return json.loads(obj_as_string)


def get_value_from_dict(keys_as_string, obj_as_dict):
    keys = keys_as_string.split('.')
    result = obj_as_dict
    for key in keys:
        result = result[key]
    return result
